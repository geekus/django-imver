from django.http import HttpResponse
from django.http import Http404
from django.core.exceptions import ImproperlyConfigured
from django.core.cache import get_cache
from django.utils.http import http_date

from imver.settings import settings
from imver.models import Image

import os


def get_image(request, image_size, image_ratio, url):
    try:
        image = Image(filename=url) # generate Image instance
    except:
        raise Http404

    version_path = image.get_version_path(size=image_size, ratio=image_ratio)

    try:
        with open(version_path, "rb") as f:
            return HttpResponse(f.read(), content_type=image.mimetype)
    except:
        raise Http404



#    stringgg = "Hello image!<br />image size: " + image_size + "<br /> iamgeratio: " + image_ratio + "<br /> path: " + image_path + "<br />"
    # response = HttpResponse(stringgg)
    # response = HttpResponse(image.version_path)
    # response = HttpResponse(img.render(), img.mimetype)
    # return response

# def _image_response(image):
#     response = HttpResponse(
#         image.render(),
#         image.mimetype
#     )
#     response['Last-Modified'] = http_date(image.modified)
#     return response


# def resize(request, path_name, format, url):
#     if path_name == 'static_resize':
#         prefix = settings.STATIC_ROOT
#     elif path_name == 'media_resize':
#         prefix = settings.MEDIA_ROOT
#     else:
#         prefix = settings.IMAGEFIT_ROOT
#     # remove prepending slash
#     if url[0] == '/':
#         url = url[1:]
#     # generate Image instance
#     image = Image(path=os.path.join(prefix, url))

#     if settings.IMAGEFIT_CACHE_ENABLED:
#         image.cache = cache
#         image.cached_name = request.META.get('PATH_INFO')
#         # shortcut everything, render cached version
#         if image.is_cached:
#             return _image_response(image)

#     # retrieve preset from format argument
#     preset = Presets.get(format) or Presets.from_string(format)
#     if not preset:
#         raise ImproperlyConfigured(
#             " \"%s\" is neither a \"WIDTHxHEIGHT\" format nor a key in " +
#             "IMAGEFIT_PRESETS." % format
#         )

#     # Resize and cache image
#     if preset.get('crop'):
#         image.crop(preset.get('width'), preset.get('height'))
#     else:
#         image.resize(preset.get('width'), preset.get('height'))
#     image.save()

#     return _image_response(image)
