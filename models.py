from __future__ import division
from imver.settings import settings
from PIL import Image as PilImage

import mimetypes
import StringIO
import re
import os

class Image(object):
    """
    Represents an Image file on the system.
    """

    def __init__(self, filename, *args, **kwargs): # , cache=None, cached_name=None

        self.filename = filename
        self.original_path = os.path.join(settings.MEDIA_ROOT, settings.IMVER_IMAGES_DIR, settings.IMVER_ORIGINALS_DIR, self.filename)
        self.original_image = PilImage.open(self.original_path)
        self.CENTER_DECIMAL = settings.IMVER_CENTER_DECIMAL

    def get_original_path(self, *args, **kwargs):
        return self.original_path

    def get_version_path(self, size, ratio, *args, **kwargs):

        self.version_size = size
        self.version_ratio = ratio

        self.version_directory = os.path.join(settings.MEDIA_ROOT, settings.IMVER_IMAGES_DIR, size + '-' + ratio)
        self.version_path = os.path.join(settings.MEDIA_ROOT, settings.IMVER_IMAGES_DIR, size + '-' + ratio , self.filename)

        if not self.version_exists:
            self.generate_version()

        return self.version_path

#         # force RGB
#         if self.pil.mode not in ('L', 'RGB'):
#             self.pil = self.pil.convert('RGB')

    def generate_version(self):

        # First, make sure there is a directory for the wanted version
        if not os.path.exists(self.version_directory):
            os.makedirs(self.version_directory)

        self.version_image = PilImage.open(self.original_path)
        self.crop()
        self.resize()


    def crop(self):

        left = 0
        upper = 0
        right = 0
        lower = 0

        # Get original image size
        orig_size_x, orig_size_y = self.version_image.size

        # Get original image aspect ratio
        orig_ratio = orig_size_x / orig_size_y

        # Get new ratio
        new_ratio_str = settings.IMVER_RATIOS[self.version_ratio]['ratio']
        new_ratio_x, new_ratio_y = new_ratio_str.split(':')
        new_ratio_x = int(new_ratio_x)
        new_ratio_y = int(new_ratio_y)

        # New ratio as a decimal, for comparing with original
        new_ratio = new_ratio_x / new_ratio_y

        # New ratio is bigger than original ratio, meaning it is wider
        if new_ratio > orig_ratio:

            left = 0
            right = orig_size_x

            new_size_x = orig_size_x
            new_size_y = int(new_size_x / new_ratio)

            center = int(orig_size_y * (1 - self.CENTER_DECIMAL))

            if (center - (new_size_y / 2)) < 0:
                upper = 0
            else:
                upper = int(center - (new_size_x / 2))
                pass

            lower = int(upper + new_size_y)

        # New ratio is smaller than original ratio, meaning it is more narrow
        elif new_ratio < orig_ratio:
            upper = 0
            lower = orig_size_y

            new_size_y = orig_size_y
            new_size_x = int(orig_size_y * new_ratio)

            center = int(orig_size_x * self.CENTER_DECIMAL)

            # Cropped version will exceed original image using suggested center
            if (center + (new_size_x / 2)) > orig_size_x:
                right = orig_size_x

            # Set right based on suggested center
            else:
                right = int(center + (new_size_x / 2))
                pass

            left = int(right - new_size_x)


        # New ratio is the same as original ratio, meaning no cropping is necessary
        else:
            return

        # print orig_ratio
        # print new_ratio
        # print new_size_x
        # print new_size_y

        # print '----------'

        # print left
        # print upper
        # print right
        # print lower

        self.version_image = self.version_image.crop((left, upper, right, lower))
        self.version_image.save(self.version_path)


    def resize(self):

        new_size_x = 0
        new_size_y = 0

        # Get old image size
        old_size_x, old_size_y = self.version_image.size

        new_max_width = settings.IMVER_SIZES[self.version_size]['width']
        new_size_x = new_max_width

        size_ratio = new_size_x / old_size_x

        # Get new ratio
        new_ratio_str = settings.IMVER_RATIOS[self.version_ratio]['ratio']
        new_ratio_x, new_ratio_y = new_ratio_str.split(':')
        new_ratio_x = int(new_ratio_x)
        new_ratio_y = int(new_ratio_y)

        # New ratio as a decimal, for comparing with original
        new_ratio = new_ratio_x / new_ratio_y

        new_size_y = int(old_size_y * size_ratio)

        new_size = (new_size_x, new_size_y)

        # print old_size_x
        # print old_size_y
        # print new_size_x
        # print new_size_y
        # print size_ratio
        # print new_size

        self.version_image = self.version_image.resize(new_size, PilImage.ANTIALIAS)
        self.version_image.save(self.version_path)

        pass


    @property
    def mimetype(self):
        return mimetypes.guess_type(self.original_path)[0]

    @property
    def version_exists(self):
        return os.path.isfile(self.version_path)
