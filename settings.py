from django.conf import settings
from django.conf import LazySettings

import tempfile
import os

class Settings(LazySettings):

    # Dict of Image Version sizes
    IMVER_SIZES = getattr(settings, 'IMVER_SIZES', {
        'tn': {'width': 50},
        'xs': {'width': 75},
        'sm': {'width': 125},
        'md': {'width': 480},
        'lg': {'width': 720},
        'xl': {'width': 1140}
    })

    # Dict of Image Version ratios
    IMVER_RATIOS = getattr(settings, 'IMVER_RATIOS', {
        'wide': {'ratio': '3:2'},
        'landscape': {'ratio': '4:3'},
        'square': {'ratio': '1:1'},
        'portrait': {'ratio': '3:4'},
        'narrow': {'ratio': '2:3'}
    })

    # What part of the image should be center by default
    # 0.5 for center, 0.618 for golden ratio
    IMVER_CENTER_DECIMAL = 0.5

    # Root images path
    IMVER_IMAGES_DIR = getattr(settings, 'IMVER_IMAGES_DIR', 'images')

    # Subdir of previous, where to find originals
    IMVER_ORIGINALS_DIR = getattr(settings, 'IMVER_ORIGINALS_DIR', 'original')

    IMVER_ORIGINALS_PATH = getattr(settings, 'IMVER_ROOT', os.path.join(IMVER_IMAGES_DIR, IMVER_ORIGINALS_DIR))

settings = Settings()
