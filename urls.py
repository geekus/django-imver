from django.conf.urls import url, patterns


urlpatterns = patterns(
    '',
    # url(
    #     r'^(?P<path_name>[\w_-]*)/(?P<format>[,\w-]+)/(?P<url>.*)/?$',
    #     'imver.views.get_image',
    #     name="imagefit_resize"),
    url(
        r'^(?P<image_size>[\w]{2})-(?P<image_ratio>[\w]*)/(?P<url>.*)/?$',
        'imver.views.get_image',
        name="imagefit_resize")
)
